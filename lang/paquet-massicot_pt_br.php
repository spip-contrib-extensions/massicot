<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-massicot?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'massicot_description' => 'Este plugin permite modificar o tamanho e reenquadrar os logotipos e as imagens diretamente no espaço privado de SPIP. ',
	'massicot_nom' => 'Massicot',
	'massicot_slogan' => 'Reenquadre suas imagens no SPIP!'
);
