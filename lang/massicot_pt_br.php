<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/massicot?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_image_trop_petite' => 'Esta imagem é muito pequena para este formato.',
	'erreur_parametre_manquant' => 'O parâmetro @parametre@ é obrigatório!',

	// L
	'label_annuler' => 'Anular',
	'label_dimensions' => 'Tamanho da imagem reenquadrada em pixels:',
	'label_format' => 'dimensões predefinidas:',

	// M
	'massicot_titre' => 'Massicot',
	'massicoter' => 'Reenquadrar a imagem',

	// O
	'operation_non_autorisee' => 'Operação não autorizada.',

	// R
	'reinitialiser' => 'Reinicializar',

	// Z
	'zoom' => 'Zoom'
);
