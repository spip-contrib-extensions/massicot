<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-massicot?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'massicot_description' => 'This plugin adds a crop & resize functionality to logos and images in the private area.',
	'massicot_nom' => 'Image cropper',
	'massicot_slogan' => 'Crop and resize your images in SPIP!'
);
